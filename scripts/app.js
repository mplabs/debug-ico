(function ( win, $, ADL ) {
  'use strict';

  var selector, option, statements, verbName, actor;

  // Extend Bootstrap's collapse feature to support
  // select inputs
  $('select[data-toggle="collapse"]').on('change', function () {

    $(this).find('option').each(function () {
      selector = this.getAttribute("href");
      $(selector).collapse( this.selected ? 'show' : 'hide' );
    });

  });

  // Append options for ADL defined statements
  $('#statements').append(function () {
    statements = $(this);

    $.each(ADL.verbs, function ( verb ) {
      verbName = verb.charAt(0).toUpperCase() + verb.slice(1);

      option = document.createElement('option');
      option.value = option.innerText = verbName

      statements.append(option);
    });
  });

  // Try to fill in some stuff from the launch parameters
  $.each(ADL.XAPIWrapper.lrs, function ( key, value ) {
    if ( ! value ) {
      return;
    }

    switch ( key ) {
      case 'endpoint':
        $('#endpointInput').val(value);
      break;

      case 'actor':
        actor = JSON.parse(value);
        if ( actor.mbox ) {
          $('#agentIdentifierInput option[value="mailbox"]')
            .attr('selected', true)
            .trigger('change');
          $('#agentMboxInput').val(actor.mbox);
        }
        if ( actor.openid ) {
          $('#agentIdentifierInput option[value="openID"]')
            .attr('selected', true)
            .trigger('change');
          $('#agentOpenidInput').val(actor.openid);
        }
        if ( actor.account ) {
          $('#agentIdentifierInput option[value="account"]')
            .attr('selected', true)
            .trigger('change');
          $('#agentAccountHomepageInput').val(actor.account.homepage);
          $('#agentAccountNameInput').val(actor.account.name);
        }
      break;

      case 'registration':
        
      break;
    }
  });

  if ( ADL.XAPIWrapper.lrs.endpoint ) {
    $('#endpointInput').val(ADL.XAPIWrapper.lrs.endpoint);
  }

})(this, jQuery, ADL);